#ifndef __symboling_ScalarFieldEmbedding__
#define __symboling_ScalarFieldEmbedding__

#include "Value.hpp"
#include "Type.hpp"
#include "ScalarField.hpp"
#include <opencv2/core/core.hpp>

namespace symboling {
  /**
   * @class ScalarFieldEmbedding
   * @description Implements a scalar-field numerical embedding (preliminary implementation).
   * - This implements the [Metrizable symbolic data structure embedding](https://www.overleaf.com/read/prntythcxgnp#42f62c) draft.
   * - A [numeric demo](https://line.gitlabpages.inria.fr/aide-group/symboling/embeddingdemo/type-field-embedding.html) is available.
   *
   * <a name="parameters"></a>
   * ## ScalarFieldEmbedding parameters
   * ### General parameter
   * - `NaN: ...` If true take also into account scalar-field points with undefined values. Default is false.
   * - `verbose: ...` If true print the interpolation, extrapolation and barycenter intermediate values. Default is false.
   * ### ScalarField parameter
   * - The underlying scalar-field parameters if the scalar-field is ommitied but defined here by its parameters.
   * @param {JSON|String} parameters The scalar-field embedding parameters.
   * @param {ScalarField|Mat} [scalarfield|distances] The scalar-field parameters or directly the distance matrix where the lower-triangle part `i < j, D[i, j]` is defined.
   * @throws illegal-argument If distance triangle inequality is not verified.
   */
  class ScalarFieldEmbedding {
    // Internal parameters
    wjson::Value parameters;
    bool NaN = false, verbose = false;
    //
    const ScalarField *scalarfield;
    ScalarField *p_scalarfield = NULL;
    std::vector < std::string > labels;
    //
    cv::Mat distances;
    unsigned int count = 0;
    double d_max = 0;
    // Generates the distance matrix from a scalarfield distance
    void scalarfield2distances(const ScalarField& scalarfield);
    // Checks the triangular inequality and completes the distance matrix
    void checkDistances(cv::Mat& distances);
    // Computes the square root of a symmetric Matrix
    static void positiveSqrtC(const cv::Mat& C, unsigned int dimension, cv::Mat& X, unsigned int& rank);
    // Returns a matrix with cols copies of a vector.
    static cv::Mat vectN2matrix(const cv::Mat& vect, unsigned int cols);
public:
    ScalarFieldEmbedding(JSON parameters, const ScalarField& scalarfield);
    ScalarFieldEmbedding(String parameters, const ScalarField& scalarfield);
    ScalarFieldEmbedding(const char *parameters, const ScalarField& scalarfield);
    ScalarFieldEmbedding(const ScalarField& scalarfield);
    ScalarFieldEmbedding(JSON parameters, const cv::Mat& distances);
    ScalarFieldEmbedding(String parameters, const cv::Mat& distances);
    ScalarFieldEmbedding(const char *parameters, const cv::Mat& distances);
    ScalarFieldEmbedding(const cv::Mat& distances);
    ScalarFieldEmbedding(JSON parameters);
    ScalarFieldEmbedding(String parameters);
    ScalarFieldEmbedding(const char *parameters);
    ~ScalarFieldEmbedding();

    /**
     * @function getParameters
     * @memberof ScalarFieldEmbedding
     * @instance
     * @description Returns the trajectory parameters.
     * @return {JSON} The trajectory parameters.
     */
    JSON getParameters() const
    {
      return parameters;
    }
    /**
     * @function setParameters
     * @memberof ScalarFieldEmbedding
     * @instance
     * @description Modifies some ScalarField parameters.
     * - All parameters but the `type` can be modified.
     * - The ScalarField points are preserved when changing the parameters, thus using the `points` field simply adds the points.
     * @param {JSON} parameters The Trajectory parameters.
     * @return {ScalarField} This value allowing to chain methods.
     */
    ScalarFieldEmbedding& setParameters(JSON parameters);

    /**
     * @function getScalarField
     * @memberof ScalarFieldEmbedding
     * @instance
     * @description Returns the underlying scalar-field.
     * @return {ScalarField} The scalar-field read-only pointer, if defined NULL otherwise.
     */
    const ScalarField *getScalarField() const
    {
      return scalarfield;
    }
    /**
     * @function getDistances
     * @memberof ScalarFieldEmbedding
     * @instance
     * @description Returns the underlying distances-field.
     * @return {Mat} The cv::Mat read-only distances matrix.
     */
    const cv::Mat& getDistances() const
    {
      return distances;
    }
#ifndef SWIG

    struct Embedding {
      // Exponent of the phi(d) : d -> d^(1/n) function, so that phi(d_ij) = ||x_i-x_j||.
      unsigned int phi_n;
      // Circumscribing hypersphere radius and vector magnitudes
      double d_c;
      // The embedding coordinates of the obtained dimension = X.rows for count = X.cols vectors.
      cv::Mat X;
      // The embedding labels
      std::vector < std::string > labels;
      // The embedding chosen dimension and obtained rank
      unsigned int dimension, rank;
    };
    mutable Embedding embedding;

    /**
     * @function getEmbedding
     * @memberof ScalarFieldEmbedding
     * @description Calculates and returns an approximate affine embedding.
     * @param {uint} dimension Affine embedding dimension. If `0`, return the minimal dimension of the best approximation.
     * @return {Embedding} The embedding as a C++ data struture.
     * ```
     * struct Embedding {
     *   // Exponent of the phi(d) : d -> d^(1/n) function, so that phi(d_ij) = ||x_i-x_j||.
     *   unsigned int phi_n;
     *   // Circumscribing hypersphere radius and vector magnitudes
     *   double d_c;
     *   // The embedding coordinates of the obtained dimension = X.rows for count = X.cols vectors.
     *   cv::Mat X;
     *   // The embedding labels
     *   std::vector <std::string> labels;
     *   // The embedding chosen dimension and obtained rank
     *   unsigned int dimension, rank;
     * };
     * ```
     */
    const Embedding& getEmbedding(unsigned int dimension = 0) const;

    struct Atlas {
      // The embedding labels
      std::vector < std::string > labels;
      // Defines a chart
      struct Chart {
        // The chart value indexes, as given by index2value
        // - The chart size is indexes.size()
        // - The chart value index is indexes.at(0)
        std::vector < unsigned int > indexes;
        // The chart value increasing distances to the chart origin
        std::vector < double > distances;
        // The chart coordinates as a size columns matrix of size-1 rows
        cv::Mat coordinates;
      };
      std::vector < Chart > charts;
      struct Transition {
        // The charts indexes of this transition map
        std::pair < unsigned int, unsigned int > charts;
        // The points indexes that are in the intersection
        std::vector < unsigned int > indexes;
        // The affine transition map, of the form X_q = matrix * X_p + vector
        cv::Mat matrix, vector;
        // Transform properties
        unsigned int affine_matrix_rank, affine_isometric_rank, rigid_matrix_rank;
      };
      std::vector < std::vector < Transition >> transitions;
    };
    mutable Atlas atlas;

    /**
     * @function getAtlas
     * @memberof ScalarFieldEmbedding
     * @description Calculates and returns the embedding atlas.
     * @return {Atlas} The embedding atlas as a C++ data struture.
     * ```
     * struct Atlas {
     *   // The embedding labels
     *   std::vector <std::string> labels;
     *   // The index <-> value correspondences, if a scalarfield is defined
     *   std::vector < wjson::Value > index2value;
     *   std::map < wjson::Value, unsigned int > value2index;
     *   // Defines a chart
     *   struct Chart {
     *	   // The chart value indexes, as given by index2value
     *	   // - The chart size is indexes.size()
     *	   // - The chart value index is indexes.at(0)
     *     std::vector < unsigned int > indexes;
     *     // The chart value increasing distances to the chart origin
     *     std::vector < double > distances;
     *     // The chart coordinates as a size columns matrix of size-1 rows
     *     cv::Mat coordinates;
     *   };
     *   std::vector < Chart > charts;
     *   struct Transition {
     *	   // The charts indexes of this transition map
     *	   std::pair < unsigned int, unsigned int > charts;
     *	   // The points indexes that are in the intersection
     *	   std::vector < unsigned int > indexes;
     *	   // The transition map, of the form X_q = matrix * X_p + vector
     *	   cv::Mat matrix, vector;
     *     // Transform properties
     *     unsigned int affine_matrix_rank, affine_isometric_rank, rigid_matrix_rank;
     *   };
     *   std::vector < std::vector < Transition >> transitions;
     * };
     * ```
     */
    const Atlas& getAtlas() const;

    /**
     * @function asJSON
     * @memberof ScalarFieldEmbedding
     * @description Returns the vector embedding and affine embedding atlas as a wJSON data structure.
     * @return {JSON} The embedding atlas as a wJSON data structure.
     */
    JSON asJSON() const;

    /**
     * @function asImage
     * @memberof ScalarFieldEmbedding
     * @description Returns the vector embedding drawn in a 2D image.
     * The `cv::imwrite("filename.png", embedding.asImage());` construct allows to save the image.
     * @return {Mat} A BGR `CV_8UC3` `cv::Mat` image with the drawn data structure.
     */
    cv::Mat asImage() const;
#endif
  };
}

#endif
