# symbolingembedding

Symbolic data structure numerical embedding

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/symbolingembedding'>https://gitlab.inria.fr/line/aide-group/symbolingembedding</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/symbolingembedding'>https://line.gitlabpages.inria.fr/aide-group/symbolingembedding</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/symbolingembedding/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/symbolingembedding/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/symbolingembedding'>softwareherirage.org</a>
- Version `0.1.0`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/symbolingembedding.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage


The installation of [opencv](https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_user) is required.

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>symbolingfield: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/symbolingfield'>Symbolic data structure variational mechanisms</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
