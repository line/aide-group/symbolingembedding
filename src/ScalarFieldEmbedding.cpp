#include "ScalarFieldEmbedding.hpp"
#include <math.h>
#include "opencv.hpp"
#include "cpp2wjson.tpp"
#include <opencv2/imgproc.hpp>

#define DEBUG

namespace symboling {
  ScalarFieldEmbedding::ScalarFieldEmbedding(JSON parameters, const ScalarField& scalarfield) : parameters(parameters), scalarfield(&scalarfield)
  {
    setParameters(parameters);
    scalarfield2distances(scalarfield);
#ifdef DEBUG
    checkDistances(distances);
#endif
  }
  ScalarFieldEmbedding::ScalarFieldEmbedding(JSON parameters, const cv::Mat& distances) : parameters(parameters), scalarfield(NULL), distances(distances)
  {
    setParameters(parameters);
    checkDistances(this->distances);
    for(unsigned int i = 0; i < count; i++) {
      labels.push_back(aidesys::echo("%d", i));
    }
  }
  ScalarFieldEmbedding::ScalarFieldEmbedding(const ScalarField& scalarfield) : ScalarFieldEmbedding(wjson::Value::EMPTY, scalarfield)
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(const cv::Mat& distances) : ScalarFieldEmbedding(wjson::Value::EMPTY, distances)
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(JSON parameters) : ScalarFieldEmbedding(parameters, *(p_scalarfield = new ScalarField(parameters)))
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(String parameters, const ScalarField& scalarfield) : ScalarFieldEmbedding(wjson::string2json(parameters), scalarfield)
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(String parameters, const cv::Mat& distances) : ScalarFieldEmbedding(wjson::string2json(parameters), distances)
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(String parameters) : ScalarFieldEmbedding(wjson::string2json(parameters))
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(const char *parameters, const ScalarField& scalarfield) : ScalarFieldEmbedding(wjson::string2json(parameters), scalarfield)
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(const char *parameters, const cv::Mat& distances) : ScalarFieldEmbedding(wjson::string2json(parameters), distances)
  {}
  ScalarFieldEmbedding::ScalarFieldEmbedding(const char *parameters) : ScalarFieldEmbedding(wjson::string2json(parameters))
  {}
  ScalarFieldEmbedding::~ScalarFieldEmbedding()
  {
    delete p_scalarfield;
  }
  ScalarFieldEmbedding& ScalarFieldEmbedding::setParameters(JSON parameters)
  {
    this->parameters.copy(parameters);
    if(p_scalarfield != NULL) {
      p_scalarfield->setParameters(parameters);
    }
    NaN = parameters.get("NaN", NaN);
    verbose = parameters.get("verbose", verbose);
    return *this;
  }
  void ScalarFieldEmbedding::scalarfield2distances(const ScalarField& scalarfield)
  {
    count = scalarfield.get().size();
    distances = cv::Mat(count, count, CV_64FC1);
    d_max = 0;
    unsigned int i = 0;
    for(auto it1 = scalarfield.get().cbegin(); it1 != scalarfield.get().cend(); it1++) {
      labels.push_back(it1->first.asString(true));
      distances.at < double > (i, i) = 0;
      if(NaN || !std::isnan(it1->second)) {
        unsigned int j = 0;
        for(auto it2 = scalarfield.get().cbegin(); it2 != it1; it2++) {
          if(NaN || !std::isnan(it2->second)) {
            double d_ij = scalarfield.getDistance(it1->first, it2->first);
            distances.at < double > (j, i) = distances.at < double > (i, j) = d_ij;
            d_max = d_max < d_ij ? d_ij : d_max;
            j++;
          }
        }
        i++;
      }
    }
  }
  void ScalarFieldEmbedding::checkDistances(cv::Mat& distances)
  {
    aidesys::alert(distances.rows != distances.cols, "illegal-argument", "in symboling::ScalarFieldEmbedding::checkDistances t distances(rows=%d, cols=%d) is not a square matrix", distances.rows, distances.cols);
    count = distances.rows;
    {
      for(unsigned int i = 0; i < count; i++) {
        distances.at < double > (i, i) = 0;
        for(unsigned int j = 0; j < i; j++) {
          double d_ij = distances.at < double > (j, i) = distances.at < double > (i, j);
          d_max = d_max < d_ij ? d_ij : d_max;
          for(unsigned int k = 0; k < count; k++) {
            aidesys::alert(distances.at < double > (i, j) > distances.at < double > (i, k) + distances.at < double > (k, j), "illegal-argument", "in symboling::ScalarFieldEmbedding::checkDistances the distance triangular inequality failed for i='%d',j='%d',k='%d': d_ij = %e > (d_ik = %e + d_jk = %e)", i, j, k, d_ij, distances.at < double > (i, k), distances.at < double > (k, j));
          }
        }
      }
    }
  }
  void ScalarFieldEmbedding::positiveSqrtC(const cv::Mat& C, unsigned int dimension, cv::Mat& X, unsigned int& rank)
  {
    cv::Mat d, R, Rt;
    cv::eigen(C, d, R);
    cv::transpose(R, Rt);
#ifdef DEBUG
    // Verifies the eigen-decomposition numerical stability
    aidesys::alert((fabs(cv::norm(Rt * R - cv::Mat::eye(C.rows, C.cols, CV_64FC1))) > 1e-6 || fabs(cv::norm(Rt * cv::Mat::diag(d) * R - C)) > 1e-6), "illegal-state", "in ScalarFieldEmbedding::positiveSqrtC bad numerical stability err = %e & %e", cv::norm(Rt * R - cv::Mat::eye(C.rows, C.cols, CV_64FC1)), cv::norm(Rt * cv::Mat::diag(d) * R - C));
#endif
    rank = 0;
    for(unsigned int i = 0; i < (unsigned int) d.rows; i++) {
      if(i < dimension && d.at < double > (i) > 0) {
        d.at < double > (i) = sqrt(d.at < double > (i));
        rank = i + 1;
      } else {
        d.at < double > (i) = 0;
      }
    }
    X = cv::Mat::diag(d) * R;
#ifdef DEBUG
    // Verifies the square root if of full rank
    if(rank == (unsigned int) d.rows) {
      cv::Mat Xt;
      cv::transpose(X, Xt);
      aidesys::alert(fabs(cv::norm(Xt * X - C)) > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getEmbedding bad numerical reconstruction err = %e", fabs(cv::norm(Xt * X - C)));
    }
#endif
  }
  const ScalarFieldEmbedding::Embedding& ScalarFieldEmbedding::getEmbedding(unsigned int dimension) const
  {
    embedding.dimension = dimension = (dimension == 0 ? count : dimension);
    embedding.labels = labels;
    // Attempts to find a phi(a) = a^1/n that allows the decomposition
    static const unsigned int MAXn = 10;
    embedding.d_c = d_max / 2;
    double d2_c = embedding.d_c * embedding.d_c;
    for(embedding.phi_n = 1; embedding.phi_n <= MAXn; embedding.phi_n++) {
      aidesys::alert(embedding.phi_n == MAXn, "illegal-state", "in ScalarFieldEmbedding::getEmbedding no convergence after %d iterations", MAXn);
      aidesys::alert(verbose, "", "in ScalarFieldEmbedding::getEmbedding phi: d -> d^1/n, n: %d", embedding.phi_n);
      // Converts distances to dotproducts
      cv::Mat C(count, count, CV_64FC1);
      for(unsigned int i = 0; i < count; i++) {
        C.at < double > (i, i) = d2_c;
        for(unsigned int j = 0; j < i; j++) {
          C.at < double > (i, j) = C.at < double > (j, i) = d2_c - pow(distances.at < double > (i, j), 2.0 / embedding.phi_n) / 2.0;
        }
      }
      // Calculates the decomposition
      positiveSqrtC(C, dimension, embedding.X, embedding.rank);
#ifdef DEBUG
      // Verifies the distances if an exact decomposition
      if(embedding.rank == count) {
        cv::Mat Err(count, count, CV_64FC1, cv::Scalar (0));
        for(unsigned int i = 0; i < count; i++) {
          for(unsigned int j = 0; j < i; j++) {
            Err.at < double > (i, j) = pow(cv::norm(embedding.X.col(i), embedding.X.col(j)), embedding.phi_n) - distances.at < double > (i, j);
          }
        }
        aidesys::alert(cv::norm(Err) > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getEmbedding bad distance mapping E:\n%s\n", opencv::mat2txt(Err).c_str());
      }
#endif
      if(embedding.rank == dimension) {
        return embedding;
      }
    }
    return embedding;
  }
  const ScalarFieldEmbedding::Atlas& ScalarFieldEmbedding::getAtlas() const
  {
    atlas.labels = labels;
    // Computes the charts
    {
      for(unsigned int p = 0; p < count; p++) {
        // Points distance sorting
        std::vector < unsigned int > sortedindex;
        std::vector < double > sorteddistances;
        // Computes the index in increasing order
        {
          std::multimap < double, unsigned int > distance2index;
          for(unsigned int q = 0; q < count; q++) {
            distance2index.insert(std::pair < double, unsigned int > (distances.at < double > (p, q), q));
          }
          for(auto it = distance2index.cbegin(); it != distance2index.cend(); it++) {
            sorteddistances.push_back(it->first);
            sortedindex.push_back(it->second);
          }
        }
        // Computes the C inner product matrix
        cv::Mat C(count, count, CV_64FC1);
        {
          for(unsigned int i = 1; i < count; i++) {
            double d_0i = distances.at < double > (p, sortedindex[i]);
            C.at < double > (i, i) = d_0i * d_0i;
            for(unsigned int j = 1; j < i; j++) {
              double d_0j = distances.at < double > (p, sortedindex[j]);
              double d_ij = distances.at < double > (sortedindex[i], sortedindex[j]);
              C.at < double > (j, i) = (d_0i * d_0i + d_0j * d_0j - d_ij * d_ij) / 2;
            }
          }
        }
        // Effective chart size
        unsigned int size = 1;
        // Local chart coordinates
        cv::Mat X(count, count, CV_64FC1, cv::Scalar (0));
        // Incrementally computes the chart coordinates
        for(unsigned int k = 1; k < count; k++) {
          for(unsigned int i = 1; i < k; i++) {
            double v = C.at < double > (i, k);
            for(unsigned int j = 1; j < i; j++) {
              v -= X.at < double > (j, i) * X.at < double > (j, k);
            }
            v = X.at < double > (i, i) > 0 ? v / X.at < double > (i, i) : 0;
            X.at < double > (i, k) = v;
          }
          double g_kk = C.at < double > (k, k);
          for(unsigned int j = 1; j < k; j++) {
            g_kk -= X.at < double > (j, k) * X.at < double > (j, k);
          }
          if(g_kk >= 0) {
            X.at < double > (k, k) = sqrt(g_kk);
            size++;
          }
          if(g_kk <= 0) {
            break;
          }
        }
        // Reduces to the chart size and register
        atlas.charts.push_back({
          { sortedindex.begin(), sortedindex.begin() + size },
          { sorteddistances.begin(), sorteddistances.begin() + size },
          X(cv::Rect(0, 1, size, size - 1))
        });
#ifdef DEBUG
        // Checks the obtained distances with respect to chart coordinates
        {
          cv::Mat Err(size, size, CV_64FC1, cv::Scalar (0));
          for(unsigned int i = 0; i < size; i++) {
            for(unsigned int j = 0; j < i; j++) {
              Err.at < double > (i, j) = cv::norm(X.col(i), X.col(j)) - distances.at < double > (sortedindex[i], sortedindex[j]);
            }
          }
          aidesys::alert(cv::norm(Err) > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas(%d) bad distance mapping E:\n%s\n", p, opencv::mat2txt(Err).c_str());
        }
#endif
      }
      // Computes the transition maps
      {
        for(unsigned int p = 0; p < count; p++) {
          std::vector < Atlas::Transition > transition_p;
          const Atlas::Chart& chart_p = atlas.charts.at(p);
          for(unsigned int q = 0; q < p; q++) {
            Atlas::Transition transition_pq;
            transition_pq.charts = std::pair < unsigned int, unsigned int > (p, q);
            const Atlas::Chart& chart_q = atlas.charts.at(q);
            // Computes the index intersection
            std::vector < unsigned int > indexes_p, indexes_q;
            {
              for(unsigned int i = 0; i < chart_p.indexes.size(); i++) {
                for(unsigned int j = 0; j < chart_q.indexes.size(); j++) {
                  if(chart_p.indexes.at(i) == chart_q.indexes.at(j)) {
                    transition_pq.indexes.push_back(chart_p.indexes.at(i));
                    indexes_p.push_back(i);
                    indexes_q.push_back(j);
                  }
                }
              }
            }
            // Computes the transformation
            {
              // Computes the intersection coordinates
              cv::Mat u(1, indexes_p.size(), CV_64FC1, cv::Scalar (1));
              cv::Mat X_p(chart_p.coordinates.rows, indexes_p.size(), CV_64FC1);
              cv::Mat X_q(chart_q.coordinates.rows, indexes_p.size(), CV_64FC1);
              {
                for(unsigned int k = 0; k < indexes_p.size(); k++) {
                  chart_p.coordinates.col(indexes_p.at(k)).copyTo(X_p(cv::Rect(k, 0, 1, chart_p.coordinates.rows)));
                  chart_q.coordinates.col(indexes_q.at(k)).copyTo(X_q(cv::Rect(k, 0, 1, chart_q.coordinates.rows)));
                }
              }
              // Computes the affine transform
              {
                cv::Mat X1_p;
                cv::vconcat(X_p, u, X1_p);
                cv::Mat U_p, d_p, Vt_p, Ut_p, V_p;
                cv::SVD::compute(X1_p, d_p, U_p, Vt_p);
                cv::transpose(U_p, Ut_p);
                cv::transpose(Vt_p, V_p);
                cv::Mat d_p1(d_p.rows, 1, CV_64FC1);
                unsigned int vector_rank = d_p.rows;
                for(int i = 0; i < d_p.rows; i++) {
                  if(fabs(d_p.at < double > (i)) < 1e-6) {
                    d_p1.at < double > (i) = 0, vector_rank--;
                  } else {
                    d_p1.at < double > (i) = pow(d_p.at < double > (i), -1);
                  }
                }
                cv::Mat A_pq = X_q * V_p * cv::Mat::diag(d_p1) * Ut_p, d_pq;
                cv::Rect matrixRect = cv::Rect(0, 0, chart_p.coordinates.rows, chart_q.coordinates.rows);
                cv::Rect vectorRect = cv::Rect(chart_p.coordinates.rows, 0, 1, chart_q.coordinates.rows);
                transition_pq.matrix = A_pq(matrixRect);
                transition_pq.vector = A_pq(vectorRect);
#ifdef DEBUG
                // Checks if points at the intersection are exactly mapped by the affine transform
                aidesys::alert(cv::norm(X_q, transition_pq.matrix * X_p + transition_pq.vector * u) > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas(%d %d) bad affine transition mapping", p, q);
#endif
                // Affine matrix properties
                {
                  cv::SVD::compute(transition_pq.matrix, d_pq);
                  transition_pq.affine_matrix_rank = d_pq.rows, transition_pq.affine_isometric_rank = 0;
                  for(int i = 0; i < d_pq.rows; i++) {
                    if(fabs(d_pq.at < double > (i)) < 1e-6) {
                      d_pq.at < double > (i) = 0, transition_pq.affine_matrix_rank--;
                    } else if(fabs(d_pq.at < double > (i) - 1) < 1e-6) {
                      d_pq.at < double > (i) = 1, transition_pq.affine_isometric_rank++;
                    }
                  }
#ifdef DEBUG
                  // Checks rank coherence between Xp and A_pq
                  aidesys::alert(transition_pq.affine_matrix_rank > vector_rank, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d incoherent rank of Xp: %d !>= A_pq: %d", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), vector_rank, transition_pq.affine_matrix_rank);
#endif
                }
                // Rigid transform estimation
                if(transition_pq.affine_isometric_rank != transition_pq.affine_matrix_rank) {
                  // Computes the relative vector
                  cv::Mat X0_p = X_p - X_p.col(0) * u;
                  cv::Mat X0_q = X_q - X_q.col(0) * u;

#ifdef DEBUG
                  // Checks that vectors are related by the affine and a rigid transform
                  {
                    double z_0 = cv::norm(X0_q - A_pq(matrixRect) * X0_p);
                    aidesys::alert(z_0 > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d incoherent vectorial relation", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), z_0);
                    cv::Mat X0t_p, X0t_q;
                    cv::transpose(X0_p, X0t_p);
                    cv::transpose(X0_q, X0t_q);
                    double z_1 = cv::norm(X0t_p * X0_p, X0t_q * X0_q);
                    aidesys::alert(z_1 > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d incoherent inter distances", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), z_1);
                  }
#endif
                  // Computes the an orthogonal matrix generalization
                  cv::Mat U0_p, Ut0_p, d0_p, Vt0_p, U0_q, Ut0_q, d0_q, Vt0_q;
                  cv::SVD::compute(X0_p, d0_p, U0_p, Vt0_p);
                  cv::transpose(U0_p, Ut0_p);
                  cv::SVD::compute(X0_q, d0_q, U0_q, Vt0_q);
                  cv::Mat R_pq = U0_q * Ut0_p;
#ifdef DEBUG
                  // Checks that X0_p and X0_q eigen decomposition are coherent
                  {
                    double z_0 = cv::norm(d0_p, d0_q), z_1 = cv::norm(Vt0_p, Vt0_q);
                    aidesys::alert(z_0 > 1e-6 || z_1 > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d incoherent X0_q versus X0_p eigen decomposition on diagonal term d = %e or right rotation Vt = %e", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), z_0, z_1);
                  }
                  // Checks the rigid vectorial mapping
                  {
                    double z = cv::norm(X0_q - R_pq * X0_p);
                    aidesys::alert(z > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d incoherent vectoral rigid mapping", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), z);
                  }
#endif
                  transition_pq.matrix = R_pq;
                  transition_pq.vector = X_q.col(0) - R_pq * X_p.col(0);
#ifdef DEBUG
                  // Checks the affine rigid mapping properties
                  {
                    cv::SVD::compute(transition_pq.matrix, d_pq);
                    unsigned int matrix_rank = d_pq.rows, isometric_rank = 0;
                    for(int i = 0; i < d_pq.rows; i++) {
                      if(fabs(d_pq.at < double > (i)) < 1e-6) {
                        d_pq.at < double > (i) = 0, matrix_rank--;
                      } else if(fabs(d_pq.at < double > (i) - 1) < 1e-6) {
                        d_pq.at < double > (i) = 1, isometric_rank++;
                      }
                    }
                    transition_pq.rigid_matrix_rank = matrix_rank;
                    aidesys::alert(transition_pq.affine_matrix_rank > matrix_rank, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d affine rank = %d incoherent with rigid matrix rank = %d d_pr = %s", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), transition_pq.affine_matrix_rank, matrix_rank, opencv::mat2txt(d_pq, true).c_str());
                    aidesys::alert(matrix_rank != isometric_rank, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d rigid matrix rank = %d incoherent with rigid matrix isometric rank = %d", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), matrix_rank, isometric_rank);
                  }
#endif
                }
                // Checks affine rigid mapping
                {
                  double z = cv::norm(X_q, transition_pq.matrix * X_p + transition_pq.vector * u);
                  aidesys::alert(z > 1e-6, "illegal-state", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d Kpq: %d incoherent affine rigid mapping", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, indexes_p.size(), z);
                }
                aidesys::alert(verbose, "", "in ScalarFieldEmbedding::getAtlas::transition(%d %d) Kp: %d Kq: %d matrix_rank: %d isometric_rank: %d not_rigid: %d affine-eigen-values: %s", p, q, chart_p.coordinates.rows, chart_q.coordinates.rows, transition_pq.affine_matrix_rank, transition_pq.affine_isometric_rank, transition_pq.affine_isometric_rank < transition_pq.affine_matrix_rank, opencv::mat2txt(d_pq, true).c_str());
              }
            }
            transition_p.push_back(transition_pq);
          }
          atlas.transitions.push_back(transition_p);
        }
      }
    }
    return atlas;
  }
  JSON ScalarFieldEmbedding::asJSON() const
  {
    static wjson::Value result;
    getEmbedding();
    result["vector-embedding"]["phi_n"] = embedding.phi_n;
    result["vector-embedding"]["d_c"] = embedding.d_c;
    result["vector-embedding"]["X"] = wjson::cpp2wjson::get(embedding.X);
    getAtlas();
    result.clear();
    for(auto it = atlas.charts.cbegin(); it != atlas.charts.cend(); it++) {
      wjson::Value chart;
      chart["size"] = it->indexes.size();
      chart["indexes"] = wjson::cpp2wjson::get(it->indexes);
      chart["distances"] = wjson::cpp2wjson::get(it->distances);
      chart["coordinates"] = wjson::cpp2wjson::get(it->coordinates);
      result["charts"].add(chart);
    }
    for(auto it = atlas.transitions.cbegin(); it != atlas.transitions.cend(); it++) {
      wjson::Value transition_p;
      for(auto jt = it->cbegin(); jt != it->cend(); jt++) {
        wjson::Value transition_pq;
        transition_pq["charts"] = wjson::cpp2wjson::get(jt->charts);
        transition_pq["indexes"] = wjson::cpp2wjson::get(jt->indexes);
        transition_pq["matrix"] = wjson::cpp2wjson::get(jt->matrix);
        transition_pq["vector"] = wjson::cpp2wjson::get(jt->vector);
        transition_pq["affine-matrix-rank"] = jt->affine_matrix_rank;
        transition_pq["affine-isometric-rank"] = jt->affine_isometric_rank;
        transition_pq["rigid-matrix-rank"] = jt->rigid_matrix_rank;
        transition_p.add(transition_pq);
      }
      result["transitions"].add(transition_p);
    }
    return result;
  }
  cv::Mat ScalarFieldEmbedding::asImage() const
  {
    static const unsigned int width = 2048, height = (3.0 / 4.0 * width), margin = 120;
    cv::Mat img(height, width, CV_8UC3, cv::Scalar (20, 20, 20));
    const cv::Mat& X = getEmbedding(2).X;
    double hoffset, hgain, voffset, vgain;
    // Computes data bounds
    {
      double hmin = DBL_MAX, hmax = -DBL_MAX, vmin = DBL_MAX, vmax = -DBL_MAX;
      for(int i = 0; i < X.cols; i++) {
        if(X.at < double > (0, i) < hmin) {
          hmin = X.at < double > (0, i);
        }
        if(X.at < double > (0, i) > hmax) {
          hmax = X.at < double > (0, i);
        }
        if(X.at < double > (1, i) < vmin) {
          vmin = X.at < double > (1, i);
        }
        if(X.at < double > (1, i) > vmax) {
          vmax = X.at < double > (1, i);
        }
      }
      hgain = hmax > hmin ? (width - 3 * margin) / (hmax - hmin) : 1;
      vgain = vmax > vmin ? (height - 2 * margin) / (vmax - vmin) : 1;
      hoffset = margin - hgain * hmin;
      voffset = margin - vgain * vmin;
    }
    // Draws the index points
    {
      static cv::Scalar colors[] = {
        cv::Scalar(255, 0, 255),
        cv::Scalar(0, 255, 255),
        cv::Scalar(255, 255, 0),
        cv::Scalar(128, 128, 255),
        cv::Scalar(128, 255, 128),
        cv::Scalar(255, 128, 128)
      };
      for(int i = 0; i < X.cols; i++) {
        cv::Point p(rint (hoffset + hgain *X.at < double > (0, i)), rint (voffset + vgain *X.at < double > (1, i)));
        cv::drawMarker(img, p, cv::Scalar(255, 255, 255));
        opencv::putText(img, labels[i], p, colors[i % 6]);
      }
    }
    return img;
  }
}
