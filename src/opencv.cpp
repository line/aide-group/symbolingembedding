#include "std.hpp"
#include "regex.hpp"
#include "opencv.hpp"
#include <opencv2/imgproc.hpp>

namespace symboling {
  cv::Mat opencv::randomOrthonormal(unsigned int rows, unsigned int cols)
  {
    cols = cols == 0 ? rows : cols;
    // Generates a random matrix
    cv::Mat mat(rows, cols, CV_64FC1);
    cv::randn(mat, cv::Scalar(0), cv::Scalar(1));
    return opencv::orthonormalizes(mat);
  }
  cv::Mat opencv::orthonormalizes(const cv::Mat& mat_, bool squaring)
  {
    cv::Mat mat;
    // Squaring if required
    if(squaring) {
      if(mat_.cols < mat_.rows) {
        cv::hconcat(mat_, cv::Mat::eye(mat_.rows, mat_.rows - mat_.cols, CV_64FC1), mat);
      }else if(mat_.cols > mat_.rows) {
        cv::vconcat(mat_, cv::Mat::eye(mat_.cols - mat_.rows, mat_.cols, CV_64FC1), mat);
      }else {
        mat = mat_.clone();
      }
    } else {
      mat = mat_.clone();
    }
    // Gram-Schmidt process
    if(mat_.cols < mat_.rows) {
      for(unsigned int i = 0; i < (unsigned int) mat.cols; i++) {
        cv::Mat u_i = mat.col(i);
        for(unsigned int j = 0; j < i; j++) {
          const cv::Mat& u_j = mat.col(j);
          u_i = u_i - u_i.dot(u_j) * u_j;
        }
        double c_ii = u_i.dot(u_i);
        if(c_ii > 0) {
          u_i /= sqrt(c_ii);
        }
        u_i.copyTo(mat.col(i));
      }
    } else {
      for(unsigned int i = 0; i < (unsigned int) mat.rows; i++) {
        cv::Mat u_i = mat.row(i);
        for(unsigned int j = 0; j < i; j++) {
          const cv::Mat& u_j = mat.row(j);
          u_i = u_i - u_i.dot(u_j) * u_j;
        }
        double c_ii = u_i.dot(u_i);
        if(c_ii > 0) {
          u_i /= sqrt(c_ii);
        }
        u_i.copyTo(mat.row(i));
      }
    }
    return mat;
  }
  unsigned int opencv::rank(const cv::Mat& mat)
  {
    cv::Mat w, u, vt;
    cv::SVD::compute(mat, w, u, vt);
    return cv::countNonZero(w > 1e-6);
  }
  // Returns the matrix data in textual wjson form
  std::string opencv::mat2txt(const cv::Mat& mat, bool oneline, bool nodata)
  {
    std::ostringstream stream;
    stream << mat;
    std::string result = aidesys::echo("{type: Mat rows: %d cols: %d", mat.rows, mat.cols) +
                         (nodata ? "}" : " values: [\n " + aidesys::regexReplace(aidesys::regexReplace(stream.str(), "]", "]\n ]}"), ";\n ", "]\n  ["));
    return oneline ? aidesys::regexReplace(result, "\\s+", " ") : result;
  }
  void opencv::putText(const cv::Mat& img, std::string text, cv::Point point, const cv::Scalar& color)
  {
    while(text.size() > 0) {
      auto pos = text.find("\n");
      std::string text_i = text.substr(0, pos);
      cv::putText(img, text_i, point, cv::FONT_HERSHEY_SIMPLEX, 1, color);
      text = pos == std::string::npos ? (std::string) "" : text.substr(pos + 1);
      point.y += 35;
    }
  }
}
