#ifndef __symboling_opencv__
#define __symboling_opencv__

#include "std.hpp"
#include <opencv2/core/core.hpp>

namespace symboling {
  /**
   * @class opencv
   * @description Specifies the opencv wrapping functions for linear and nonlinear solvers.
   * - These functions are accessible via the `symboling::` prefix.
   * - The basic object is a `Matrix` (including 1D vectors and co-vectors) of double 2D arrays, e.g.:
   * ```
   * cv::Mat mat = cv::Mat(rows, cols, CV_64FC1, cv::Scalar(0));
   * // Element access writes:
   * double v = mat.at<double>(row_num, col_num)
   * // Submatrix extraction writes:
   * cv:Mat submat = mat(cv:Rect(col_num, row_num, col_count, row_count));
   * ```
   */
  class opencv {
public:

    /**
     * @function randomOrthonormal
     * @memberof opencv
     * @static
     * @description Returns a random orthonormal matrix.
     * @param {uint} rows The dimension of the random vectors.
     * @param {uint} cols The number of vectors, default is equal to `rows`.
     * @return {Matrix} A random orthonormal matrix.
     */
    static cv::Mat randomOrthonormal(unsigned int rows, unsigned int cols = 0);

    /**
     * @function orthonormalizes
     * @memberof opencv
     * @static
     * @description Returns a random orthonormal matrix.
     * - If `cols` < `rows` The rectangular matrix is made of `cols` unary orthogonal vectors of dimension `rows`.
     * - If `rows` < `cols` The rectangular matrix is made of `rows` unary orthogonal vectors of dimension `cols`.
     * - A standard Gram-Schmidt process is used, preserving the 1st vectors coordinates if already orthonormal.
     * @param {Matrix} mat The input matrix.
     * @param {bool} [squaring = false] If true the matrix is concatenated in square matrix adding ones on the main diagonal and zeros elsewhere.
     * @return {Matrix} An orthonormal matrix.
     */
    static cv::Mat orthonormalizes(const cv::Mat& mat, bool squaring = false);

    /**
     * @function rank
     * @memberof opencv
     * @static
     * @description Returns the rank of the matrix.
     * @param {Mat} mat The input matrix.
     * @return {unint} The matrix rank.
     */
    static unsigned int rank(const cv::Mat& mat);

    /**
     * @function mat2txt
     * @memberof opencv
     * @static
     * @description Returns the image data in textual form using wJSON syntax.
     * @param {Mat} mat The input matrix.
     * @param [oneline = false] If true returns a one line string.
     * @param [nodata = false] If true returns a one line string without the matrix elements but its size only.
     * @return {String} A string with a 2D print of the image pixel values.
     */
    static std::string mat2txt(const cv::Mat& mat, bool oneline = false, bool nodata = false);

    /**
     * @function putText
     * @memberof opencv
     * @static
     * @description Write a multiline string in an image
     * @param {Mat} img The input matrix.
     * @param {String} text The multiline text
     * @param {Point} point Bottom-left corner of the text string in the image.
     * @return {Scalar} color The text color.
     */
    static void putText(const cv::Mat& img, std::string text, cv::Point point, const cv::Scalar& color);
  };
}
#endif
